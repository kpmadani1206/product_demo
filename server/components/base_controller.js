const {Express} = require('express');
const {Middleware} = require('../utils');

class BaseController{
    app;
    root = '';
    middleware;
    constructor(app, module) {
        this.app = app;
        this.root = module;
        this.middleware = new Middleware();
    }

    get isAuthRequired() {
        return true;
    }

    getBaseRoute(path = null) {
        return (this.root ? this.root  : '') + (this.name && this.name !== 'index' ? '/' + this.name : '') + (path ? '/' + path : '');
    }
    /**
     * @returns {string}
     */
    toDashCash(str) {
        return str
            .replace(/(?:^|\.?)([A-Z])/g, function(x, y) {
                return '-' + y.toLowerCase();
            })
            .replace(/^-/, '');
    }
    get name() {
        return this.toDashCash(this.constructor.name.replace('Controller', ''));
    }

    async route(method, path, handler, isAuthRequired) {
        let route = path;
        route = '/api/v' + this.app.apiVersion + '/' + route;
        console.log('Route:', route, ',Action Type:', method, ',isAuthRequired:', isAuthRequired);
        const handle = async (req, res) => {
            try {
                if(isAuthRequired){
                    let token = req.headers['x-access-token'] || req.headers['authorization'];
                    const isAuthenticated = await this.middleware.checkToken(token);
                    if(isAuthenticated){
                        const resp = await handler(req);
                        res.status(200).send({success: true,data:resp});
                    }else{
                        res
                            .status(403)
                            .send({error: 'Invalid Authorization'});
                    }
                }else{
                    const resp = await handler(req);
                    res.status(200).send({success: true,data:resp});
                }
            } catch (e) {
                console.error({e});
                res
                    .status(500)
                    .send({success: false, error: 'something went wrong!!'});
            }
        };
        this.app.webServer[method](route + '', handle );
    }

    init(methods) {
        if ( !methods ){
            console.error('please define methods in routes.', this.name);
            return;
        }
        let props = [], obj = this;
        do {
            for (const k of Object.getOwnPropertyNames(obj)) {
                if (typeof this[k] == 'function') {
                    props.push(k);
                }
            }
        } while (obj = Object.getPrototypeOf(obj));

        for (const k of props) {
            if(k.indexOf('action')===0){
                const name = k.substr(6);
                const actionType = methods[k] || 'get';
                this.route(actionType.action.toLowerCase(), this.toDashCash(name), (this[k]).bind(this), !actionType.visitorsOnly);
            }
        }
    }
}
module.exports = {BaseController};