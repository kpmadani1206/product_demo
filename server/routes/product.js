const {BaseController} = require('../components/base_controller');
class Product extends BaseController{

    methods = {
        actionGetProducts: {
            action: 'get',
        },
        actionSaveProduct: {
            action: 'post',
        },
        actionDeleteProduct: {
            action: 'delete',
        }
    };

    async actionGetProducts(req){
        const {page = 0, limit = 5} = req.query;
        const skip = ((page-1)*limit);
        const totalCountQuery = "select count(id) as count from product";
        const selectQuery = "select p.*,u.name as ownerName from product p left join user u on p.owner = u.id limit ? offset ?";
        const result = await this.app.db.query(selectQuery, [limit, skip]);
        const totalCountResult = await this.app.db.query(totalCountQuery);
        if (result.length > 0) {
            return { success: true, products: result, totalCount: totalCountResult[0] ? totalCountResult[0].count : 1};
        } else {
            return { success: false };
        }
    }

    async actionSaveProduct(req){
        const {name,productCode,price,category,manufactureDate,expiryDate,status,owner} = req.body.product;
        const insertQuery = "insert into product (name,productCode,price,category,manufactureDate,expiryDate,status,owner) values (?,?,?,?,?,?,?,?)";
        const result = await this.app.db.query(insertQuery, [name,productCode,price,category,manufactureDate,expiryDate,status,owner]);
        if (result.length > 0) {
            return { success: true };
        } else {
            return { success: false };
        }
    }
    async actionDeleteProduct(req){
        const {productId} = req.query;
        if ( !productId ) return { success: false, msg: 'Please provide product id' };
        const deleteQuery = "delete from product where id = ?";
        const result = await this.app.db.query(deleteQuery, [productId]);
        if (result.length > 0) {
            return { success: true };
        } else {
            return { success: false };
        }
    }
}
exports.Product = Product;