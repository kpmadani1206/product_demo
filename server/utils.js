let jwt = require('jsonwebtoken');
let config =  require('./config/config');
class Middleware{
    async checkToken(token){
        return new Promise((resolve, reject) => {
            if (token && token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }
            if (token) {
                jwt.verify(token, config.secret, (err, data) => {
                    if (err) {
                        resolve(false);
                    } else {
                        resolve(data);
                    }
                });
            } else {
                resolve(false);
            }
        })
    }
}
module.exports = {Middleware};