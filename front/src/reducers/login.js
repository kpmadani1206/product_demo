const reducer = (state = {
    user: undefined,
    isAuthenticated: false,
    isError: false,
    errorMessage: '',
    isLoading: false,
    token: '',
}, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            if (!action.response.data.success)
                return { ...state, isError: true, errorMessage: action.response.data.msg,isLoading: false };
            else
                return { ...state, isAuthenticated: true, isError: false, errorMessage: '', user: action.response.data.user, isLoading: false, token: action.response.data.token };
        case 'LOGIN_REQUEST':
            return { ...state, isLoading: true, isError: false, errorMessage: '' };
        case 'AUTHENTICATION_REQUEST':
            return { ...state, isLoading: true, isError: false, errorMessage: '' };
        case 'AUTHENTICATION_COMPLETED':
            if (!action.response.data.success)
                return { ...state, isError: true, errorMessage: action.response.data.msg,isLoading: false };
            else
                return { ...state, isAuthenticated: true, isError: false, errorMessage: '', user: action.response.data.user, isLoading: false, token: action.response.data.token };
        case 'LOGOUT':
            return { ...state, user: undefined, isAuthenticated: false, token: '' };
        default:
            return state;
    }
};
export default reducer;