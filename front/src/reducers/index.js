import { combineReducers as combine } from 'redux';
import products from './products';
import login from './login';
export default combine({
    products,
    login,
});