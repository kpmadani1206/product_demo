const reducer = (state = {
    products: [],
    loading: false,
    isError: false,
    saveSuccess: false,
    totalCount: 0,
}, action) => {
    switch (action.type) {
        case 'PRODUCTS_RECEIVED':
            if ( action.response.success )
                return { ...state,saveSuccess: false, isError: false, loading: false, products: action.response.data.products, totalCount: action.response.data.totalCount };
            else
                return { ...state, isError: false, loading: false };
        case 'SAVE_PRODUCT':
            return { ...state, loading: true, saveSuccess: false };
        case 'SAVE_PRODUCT_RECEIVED':
            if ( action.response.success )
                return { ...state, isError: false, loading: false, saveSuccess: true };
            else
                return { ...state, isError: true, loading: false, saveSuccess: true };
        case 'DELETE_PRODUCT_SUCCESS':
            const productIndex = state.products.findIndex(p => p.id === action.response);
            const products = [...state.products];
            let totalCount = state.totalCount;
            if ( productIndex > -1 ) {
                products.splice(productIndex, 1);
                totalCount--;
            }
            return { ...state, isError: true, loading: false,products, totalCount };
        default:
            return state;
    }
};
export default reducer;