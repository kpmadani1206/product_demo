import React, {useEffect} from 'react';
import {
    Redirect,
    Route
} from "react-router-dom";
import {connect} from 'react-redux';
import {getUserAuthenticated, getUser} from "../../../selectors/login";
import {doAuthentication} from "../../../actions/login";
const PrivateRoute = ({ children, ...rest }) => {
    const {isAuthenticated, user, doAuthentication} = {...rest};
    return (
        <Route {...rest} render={() => {
            return isAuthenticated
                ? children
                : <Redirect to='/login' />
        }} />
    )
}
const mapStateToProps = state => {
    const isAuthenticated = getUserAuthenticated(state);
    const user = getUser(state);
    return {
        isAuthenticated,
        user,
    }
}
export default connect(mapStateToProps, {doAuthentication})(PrivateRoute);