import React, {useEffect, useState} from 'react';
import './style.css';
import {connect} from "react-redux";
import {doLogin} from "../../actions/login";
import {getErrorMessage, getIsError, getIsLoading, getToken, getUserAuthenticated} from "../../selectors/login";
import { useHistory } from "react-router-dom";
const Login = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const {isError, errorMessage, isLoading, isUserAuthenticated, token} = props;
    const history = useHistory();
    useEffect(() => {
        if ( isError )
            alert(errorMessage);
    }, [isError]);
    useEffect(() => {
        if ( !isLoading && !isError && isUserAuthenticated) {
            localStorage.setItem('product_user_session', token);
            history.push("/");
        }
    }, [isUserAuthenticated]);
    return <div className={'container'}>
        <h3>Login</h3>
        <input className={'input'} type={'text'} name={'email'} placeholder={'Enter Email'} onChange={(e) => {
            setEmail(e.target.value);
        }}/>
        <input className={'input'} type={'password'} name={'password'} placeholder={'Enter Password'} onChange={(e) => {
            setPassword(e.target.value);
        }}/>
        <button className={isLoading ? 'disabled' : ''} disabled={isLoading} onClick={() => {
            if ( !email ) {
                alert('Please Enter Email!!!');
                return;
            }
            if ( !password ) {
                alert('Please Enter Password!!!');
                return;
            }
            props.doLogin({
                email,
                password,
            })
        }}>
            Login
        </button>
    </div>
}
const mapStateToPros = state => {
    const isError = getIsError(state);
    const errorMessage = getErrorMessage(state);
    const isLoading = getIsLoading(state);
    const isUserAuthenticated = getUserAuthenticated(state);
    const token = getToken(state);
    return {
        isError,
        errorMessage,
        isLoading,
        isUserAuthenticated,
        token,
    }
}
export default connect(mapStateToPros, {doLogin})(Login);