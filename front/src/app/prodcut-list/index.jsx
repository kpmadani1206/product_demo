import React, {useEffect, useState} from 'react';
import './style.css';
import {Link, useHistory} from "react-router-dom";
import {getProducts, deleteProduct} from '../../actions/products';
import {connect} from "react-redux";
import {getProductsList, getTotalCount} from "../../selectors/products";
import {getUser, getUserAuthenticated} from "../../selectors/login";
import {doLogout} from "../../actions/login";
const ProductList = (props) => {
    const {products, user, doLogout, totalCount, deleteProduct} = props;
    const pages = Math.ceil((totalCount / 5));
    const [currentPage, setCurrentPage] = useState(1);
    const history = useHistory();
    useEffect(() => {
        getProducts();
    },[currentPage]);
    const getProducts = () => {
        props.getProducts({page: currentPage});
    }
    return <div>
        <div className={'header'}>
            <h3>Product List</h3>
            <Link to={'/product'}>Add New Product</Link>
            <button onClick={()=>{
                localStorage.removeItem('product_user_session');
                doLogout();
                setTimeout(()=> {
                    history.push("/login");
                },1000);
            }}>Logout</button>
        </div>
        <div>
            {
                products.length === 0 ?
                    "No Products Created."
                    :
                    <div className={'list-container'}>
                        <table>
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Product Name</td>
                                <td>Product Code</td>
                                <td>Price</td>
                                <td>Category</td>
                                <td>Manufacture Date</td>
                                <td>Expiry Date</td>
                                <td>Status</td>
                                <td>Owner</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                products.map((product, index) => {
                                    return <tr key={index}>
                                        <td>
                                            {product.id}
                                        </td>
                                        <td>
                                            {product.name}
                                        </td>
                                        <td>
                                            {product.productCode}
                                        </td>
                                        <td>
                                            {product.price}
                                        </td>
                                        <td>
                                            {product.category}
                                        </td>
                                        <td>
                                            {product.manufactureDate}
                                        </td>
                                        <td>
                                            {product.expiryDate}
                                        </td>
                                        <td>
                                            {product.status}
                                        </td>
                                        <td>
                                            {product.ownerName}
                                        </td>
                                        <td>
                                            <button onClick={() => {
                                                if ( product.owner !== user.id ) {
                                                    alert('This product is owned by other user.You cannot delete');
                                                    return;
                                                }
                                                deleteProduct(product.id);
                                            }}>Delete</button>
                                        </td>
                                    </tr>
                                })
                            }
                            </tbody>
                        </table>
                        <div className={'pagination'}>
                            <button onClick={()=>{
                                setCurrentPage(currentPage-1);
                            }} disabled={currentPage === 1} className={currentPage === 1 ? 'disabled': ''}>Previous</button>
                            <button onClick={()=>{
                                setCurrentPage(currentPage+1);
                            }} disabled={currentPage >= pages} className={currentPage >= pages ? 'disabled': ''}>Next</button>
                            <h4>Total Records : {totalCount}</h4>
                        </div>
                    </div>
            }
        </div>
    </div>
}
const mapStateToProps = state => {
    return {
        products: getProductsList(state),
        user: getUser(state),
        totalCount: getTotalCount(state),
        isAuthenticated: getUserAuthenticated(state),
    }
}
export default connect(mapStateToProps, {getProducts, doLogout, deleteProduct})(ProductList);
