import React, {useEffect, useState} from 'react';
import './style.css';
import {connect} from "react-redux";
import {doLogin} from "../../actions/login";
import {
    getUser,
} from "../../selectors/login";
import {
    getError,
    getIsLoading,
    getSaveSuccess,
} from "../../selectors/products";
import moment from "moment";
import {saveProduct} from "../../actions/products";
import {useHistory} from "react-router-dom";
const Product = (props) => {
    const history = useHistory();
    const {user, isLoading, isError, saveSuccess} = props;
    const [product, setProduct] = useState({
        name:'',
        productCode:'',
        price: 0,
        category:'',
        manufactureDate:moment().format('yyyy-MM-DD'),
        expiryDate: moment().format('yyyy-MM-DD'),
        status: 'active',
        owner: user.id,
    });
    useEffect(() => {
        if ( isError ) {
            alert('Error while saving product');
        } else if ( saveSuccess){
            history.push("/");
        }
    }, [saveSuccess]);
    const onChange = (event) => {
        const copy = {...product};
        copy[event.target.name] = event.target.value;
        setProduct(copy);
    }
    const onSaveProduct = () => {
        let errors = '';
        Object.keys(product).forEach(key => {
           if ( !product[key] ) errors += '\nPlease enter ' + key;
        });
        if ( errors) {
            alert(errors);
            return;
        }
        props.saveProduct(product);
    }
    return <div className={'container'}>
        <h3>Create Product</h3>
        <label htmlFor={'name'}>Enter Name</label>
        <input value={product.name} className={'input'} type={'text'} name={'name'} placeholder={'Enter Product Name'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'productCode'}>Enter Product Code</label>
        <input value={product.productCode}  className={'input'} type={'text'} name={'productCode'} placeholder={'Enter Product Code'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'price'}>Enter Price</label>
        <input value={product.price}  className={'input'} type={'number'} name={'price'} placeholder={'Enter Price'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'category'}>Enter Category</label>
        <input value={product.category}  className={'input'} type={'text'} name={'category'} placeholder={'Enter Category'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'manufactureDate'}>Enter Manufacture Date</label>
        <input value={product.manufactureDate}  className={'input'} type={'date'} name={'manufactureDate'} placeholder={'Enter Manufacture Date'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'expiryDate'}>Enter Expiry Date</label>
        <input value={product.expiryDate}  className={'input'} type={'date'} name={'expiryDate'} placeholder={'Enter Expiry Date'} onChange={(e) => {
            onChange(e);
        }}/>
        <label htmlFor={'status'}>Status of Product</label>
        <input checked={product.status === 'active'}  className={'input'} type={'checkbox'} name={'status'} placeholder={'Enter status'} onChange={(e) => {
            const copy= {...product};
            copy.status = e.target.checked ? 'active' : 'disabled';
            setProduct(copy);
        }}/>
        <button disabled={isLoading} className={isLoading ? 'disabled' : ''} onClick={onSaveProduct}>
            Add Product
        </button>
    </div>
}
const mapStateToPros = state => {
    const user = getUser(state);
    const isLoading = getIsLoading(state);
    const isError = getError(state);
    const saveSuccess = getSaveSuccess(state);
    return {
        user,
        isLoading,
        isError,
        saveSuccess,
    }
}
export default connect(mapStateToPros, {doLogin, saveProduct})(Product);