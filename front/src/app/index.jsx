import React, {useEffect} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route, Link
} from "react-router-dom";
import './style.css';
import PrivateRoute from "./components/common/PrivateRoute.jsx";
import ProductList from "./prodcut-list/index.jsx";
import Login from "./login/index.jsx";
import {connect} from "react-redux";
import {doAuthentication} from "../actions/login";
import {getUser} from "../selectors/login";
import Product from "./prodcut-list/product.jsx";
const App = (props) => {
    const {user, doAuthentication} = props;
    useEffect(() => {
        if ( !user ) {
            const token = localStorage.getItem('product_user_session');
            doAuthentication(token);
        }
    }, []);
    return <Router>
            <div>
                <div className='main-header'>
                    <h1>Product Demo</h1>
                    {user && <h3>Welcome, {user ? user.name : ''}</h3>}
                </div>
                <Switch>
                    <Route exact path="/login">
                        <Login/>
                    </Route>
                    <PrivateRoute exact path="/">
                        <ProductList/>
                    </PrivateRoute>
                    <PrivateRoute exact path="/product">
                        <Product/>
                    </PrivateRoute>
                </Switch>
            </div>
    </Router>
}
const mapStateToProps = state => {
    const user = getUser(state);
    return {
        user,
    }
}
export default connect(mapStateToProps, {doAuthentication})(App);