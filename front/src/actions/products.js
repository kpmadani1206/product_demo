export const getProducts = (data) => ({
    type: 'GET_PRODUCTS',
    data,
});
export const saveProduct = (product) => ({
    type: 'SAVE_PRODUCT',
    product
});
export const deleteProduct = (productId) => ({
    type: 'DELETE_PRODUCT',
    productId
});