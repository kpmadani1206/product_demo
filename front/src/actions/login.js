export const doLogin = (data) => ({
    type: 'LOGIN_REQUEST',
    data,
});
export const doAuthentication = (data) => ({
    type: 'AUTHENTICATION_REQUEST',
    data,
});
export const doLogout = () => ({
    type: 'LOGOUT',
});