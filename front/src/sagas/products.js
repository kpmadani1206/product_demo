import 'regenerator-runtime/runtime'

import { put, takeLatest, all } from 'redux-saga/effects';
function* fetchProducts(data) {
    const {page} = data.data;
    const json = yield fetch('http://localhost:3000/api/v1/get-products?page='+page,{
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+localStorage.getItem('product_user_session'),
        }
    })
        .then(response => response.json());
    yield put({ type: "PRODUCTS_RECEIVED", response: json, });
}
function* actionProductsWatcher() {
    yield takeLatest('GET_PRODUCTS', fetchProducts)
}
function* saveProduct(product) {
    const json = yield fetch('http://localhost:3000/api/v1/save-product',{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+localStorage.getItem('product_user_session'),
        },
        body: JSON.stringify(product)
    }).then(response => response.json());
    yield put({ type: "SAVE_PRODUCT_RECEIVED", response: json, });
}
function* actionSaveProductWatcher() {
    yield takeLatest('SAVE_PRODUCT', saveProduct)
}
function* deleteProduct(data) {
    const json = yield fetch('http://localhost:3000/api/v1/delete-product?productId='+data.productId,{
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+localStorage.getItem('product_user_session'),
        },
    }).then(response => response.json());
    yield put({ type: "DELETE_PRODUCT_SUCCESS", response: data.productId, });
}
function* actionDeleteProductWatcher() {
    yield takeLatest('DELETE_PRODUCT', deleteProduct)
}
export default function* rootSaga() {
    yield all([
        actionProductsWatcher(),
        actionSaveProductWatcher(),
        actionDeleteProductWatcher(),
    ]);
}