import 'regenerator-runtime/runtime'

import { put, takeLatest, all } from 'redux-saga/effects';
function* doLogin(data) {
    const json = yield fetch('http://localhost:3000/api/v1/login',{
        method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json());
    yield put({ type: "LOGIN_SUCCESS", response: json });
}
function* actionDoLoginWatcher() {
    yield takeLatest('LOGIN_REQUEST', doLogin)
}
function* doAuthentication(data) {
    const json = yield fetch('http://localhost:3000/api/v1/authenticate',{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(response => response.json());
    yield put({ type: "AUTHENTICATION_COMPLETED", response: json });
}
function* actionDoAuthentication() {
    yield takeLatest('AUTHENTICATION_REQUEST', doAuthentication)
}
export default function* rootSaga() {
    yield all([
        actionDoLoginWatcher(),
        actionDoAuthentication(),
    ]);
}