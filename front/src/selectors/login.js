export function getUser( state ) {
    return state.login.user;
}
export function getUserAuthenticated( state ) {
    return state.login.isAuthenticated;
}
export function getIsError( state ) {
    return state.login.isError;
}
export function getErrorMessage( state ) {
    return state.login.errorMessage;
}
export function getIsLoading( state ) {
    return state.login.isLoading;
}
export function getToken( state ) {
    return state.login.token;
}