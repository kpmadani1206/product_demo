export function getProductsList( state ) {
    return state.products.products || [];
}
export function getIsLoading( state ) {
    return state.products.loading;
}
export function getError( state ) {
    return state.products.isError;
}
export function getSaveSuccess( state ) {
    return state.products.saveSuccess;
}
export function getTotalCount( state ) {
    return state.products.totalCount;
}