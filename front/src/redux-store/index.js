import { createStore, applyMiddleware } from 'redux';
import { put, takeLatest, all } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';
import reducer from '../reducers/index';
import products from '../sagas/products';
import login from '../sagas/login';
// create the saga middleware
const sagaMiddleware = createSagaMiddleware()
// mount it on the Store
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
)
function* rootSaga() {
  yield all([
    products(),
    login()
  ])
}
// then run the saga
sagaMiddleware.run(rootSaga);
export default store;
